import {Component} from '@angular/core';
import {Routes} from '@angular/router';

/* App components */
import {NotFoundComponent} from './common/components/404/404.component';
import {AppComponent} from './components/root/app.component';

export const declarations: Array<Component> = [
  NotFoundComponent,
  AppComponent
];

export const routes: Routes = [
  {
    path: '',
    loadChildren: './pages/home/module'
  },
  {
    path: 'products',
    loadChildren: './pages/product/module'
  },
  {
    path: 'cart',
    loadChildren: './pages/cart/module'
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: '404'
  }
];
