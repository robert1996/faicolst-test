import {Component} from '@angular/core';
import {Routes} from '@angular/router';

/* Product component */
import {CartComponent} from './components/root/cart.component';

export const declarations: Array<Component> = [
  CartComponent
];

export const routes: Routes = [
  {
    path: '',
    component: CartComponent
  }
];
