import {CartModule} from '../../modules/cart/module';
import {routes, declarations} from './router';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';

import {ComponentsModule} from '../../common/components.module';

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    ComponentsModule,
    CommonModule,
    CartModule
  ],
  declarations: [].concat(declarations)
})
export default class CartPageModule {
}
