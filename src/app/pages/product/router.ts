import {Component} from '@angular/core';
import {Routes} from '@angular/router';

/* Product component */
import {ProductDetailsComponent} from '../../modules/product/components/details/details.component';
import {ProductComponent} from './components/root/product.component';


export const declarations: Array<Component> = [
  ProductComponent
];

export const routes: Routes = [
  {
    path: '',
    component: ProductComponent
  },
  {
    path: 'details/:id',
    component: ProductDetailsComponent
  },
];
