import {ProductModule} from '../../modules/product/module';
import {routes, declarations} from './router';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';

import {ComponentsModule} from '../../common/components.module';

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    ComponentsModule,
    ProductModule,
    CommonModule
  ],
  declarations: [].concat(declarations)
})
export default class ProductPageModule {
}
