import {Component} from '@angular/core';
import {Routes} from '@angular/router';

/* Home components */
import {CarouselComponent} from './components/carousel/carousel.component';
import {HomeComponent} from './components/root/home.component';

export const declarations: Array<Component> = [
  CarouselComponent,
  HomeComponent
];

export const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  }
];
