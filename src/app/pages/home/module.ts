import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {CollectionsModule} from '../../modules/collections/module';
import {ComponentsModule} from '../../common/components.module';
import {ProductModule} from '../../modules/product/module';
import {CartModule} from '../../modules/cart/module';
import {routes, declarations} from './router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ComponentsModule,
    CollectionsModule,
    ProductModule,
    CartModule
  ],
  declarations: [].concat(declarations)
})
export default class HomePageModule {
}
