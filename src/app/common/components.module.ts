import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

/* Partials */
import {FooterComponent} from './components/partials/footer/footer.component';
import {HeaderComponent} from './components/partials/header/header.component';
import {ShopComponent} from '../modules/shop/components/shop/shop.component';

const components: Array<any> = [
  HeaderComponent,
  FooterComponent,
  ShopComponent
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [components],
  exports: [components]
})
export class ComponentsModule {
}
