import {NgReduxModule, NgRedux} from '@angular-redux/store';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {NgModule} from '@angular/core';

import {ComponentsModule} from './common/components.module';

import {IAppState, INITIAL_STATE, rootReducer} from './state/store';
import {AppComponent} from './components/root/app.component';
import {routes, declarations} from './app.router';

@NgModule({
  declarations: [].concat(declarations),
  imports: [
    ComponentsModule,
    NgReduxModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes)
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(ngRedux: NgRedux<IAppState>) {
    ngRedux.configureStore(rootReducer, INITIAL_STATE);
  }
}
