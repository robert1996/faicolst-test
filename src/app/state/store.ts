export interface IAppState {
  title: string;
}

export const INITIAL_STATE: IAppState = {
  title: 'KES'
};

export const actions: any = {
  ASSIGN: 'assign'
};

export const rootReducer = (state: IAppState, action: any) => {
  switch (action.type) {
    case actions.ASSIGN:
      return Object.assign({...state}, action.payload);
    default:
      return state;
  }
};
