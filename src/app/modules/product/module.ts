import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

import {ComponentsModule} from '../../common/components.module';

/* Product components */
import {ProductDetailsComponent} from './components/details/details.component';
import {ProductBadgeComponent} from './components/badge/badge.component';
import {ProductInfoComponent} from './components/info/info.component';
import {ProductViewComponent} from './components/view/view.component';

const declarations = [
  ProductDetailsComponent,
  ProductBadgeComponent,
  ProductInfoComponent,
  ProductViewComponent
];

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: declarations,
  exports: declarations
})
export class ProductModule {
}
