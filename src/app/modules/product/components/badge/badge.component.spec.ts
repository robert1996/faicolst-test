import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductBadgeComponent } from './badge.component';

describe('ProductBadgeComponent', () => {
  let component: ProductBadgeComponent;
  let fixture: ComponentFixture<ProductBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductBadgeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
