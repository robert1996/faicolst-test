import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

/* Cart components */
import {CartOverviewComponent} from './components/overview/overview.component';
import {CartWindowComponent} from './components/window/window.component';

const declarations = [
  CartOverviewComponent,
  CartWindowComponent
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: declarations,
  exports: declarations
})
export class CartModule {
}
