import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

/* New product components */
import {ProductOffersComponent} from './components/product-offers/product-offers.component';
import {NewProductsComponent} from './components/new-products/new-products.component';

const declarations = [
  ProductOffersComponent,
  NewProductsComponent
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [declarations],
  exports: [declarations]
})
export class CollectionsModule {
}
