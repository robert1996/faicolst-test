import { ShopComponent } from './components/shop/shop.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

const declarations = [
  ShopComponent
];

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: declarations,
  exports: declarations
})
export class ShopModule { }
