import {select, NgRedux} from '@angular-redux/store';
import {actions, IAppState} from '../../state/store';
import {Observable} from 'rxjs/Observable';
import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  @select() title$: Observable<string>;

  constructor(private ngRedux: NgRedux<IAppState>) {
  }

  public changeTitle() {
    this.ngRedux.dispatch({type: actions.ASSIGN, payload: {title: 'Kichink Expert Services'}});
  }
}
