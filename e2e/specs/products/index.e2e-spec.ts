import {ProductsPage} from '../../pages/products';
import {browser} from 'protractor';

describe('ProductsPage', () => {
  let productPage: ProductsPage;

  beforeEach(() => {
    productPage = new ProductsPage();
    productPage.navigateTo(productPage.getAbsoluteUrl());
  });

  it('should display the product image', () => {
    const image = productPage.getImage();
    expect(image.isDisplayed()).toBeTruthy();
  });

  it('should display the transparent overlay on mouseover', () => {
    const image = productPage.getImage();
    const hover = productPage.getHover();
    const button = productPage.getHoverButton();
    browser.actions().mouseMove(image).perform();
    expect(hover.isDisplayed()).toBeTruthy();
    expect(button.isDisplayed()).toBeTruthy();
  });

  it('should display the product title', () => {
    const title = productPage.getImageTitle();
    expect(title.isDisplayed()).toBeTruthy();
  });

  it('should display the product price', () => {
    const price = productPage.getPrice();
    expect(price.isDisplayed()).toBeTruthy();
  });

  it('should display the product rate', () => {
    const rating = productPage.getRating();
    expect(rating.isDisplayed()).toBeTruthy();
  });
});
