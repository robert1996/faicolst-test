import {DetailsPage} from '../../pages/detail';

describe('DetailsPage', () => {
  let detailsPage: DetailsPage;

  beforeEach(() => {
    detailsPage = new DetailsPage();
    detailsPage.navigateTo(detailsPage.getAbsoluteUrl());
  });

  it('should display items miniatures', () => {
    const images = detailsPage.getItemMiniatures();
    expect(images.count()).toEqual(3);
  });

  it('should display item image', () => {
    const image = detailsPage.getItemImage();
    expect(image.isDisplayed()).toBeTruthy();
  });

  it('should display Related products section', () => {
    const relatedProducts = detailsPage.getRelatedProcuctsSection();
    expect(relatedProducts.isDisplayed()).toBeTruthy();
  });
});
