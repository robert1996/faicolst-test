import {Header} from '../../../pages/partials/header';
import {browser} from 'protractor';

describe('Header', () => {
  let header: Header;

  beforeEach(() => {
    header = new Header();
    header.navigateTo(header.getAbsoluteUrl());
  });

  it('should display the logo', () => {
    const logo = header.getLogo();
    expect(logo.isDisplayed()).toBeTruthy();
  });

  it('should display header items', () => {
    const headerItems = header.getHeaderItems();
    headerItems.each(function (element) {
      expect(element.isDisplayed()).toBeTruthy();
    });
  });

  it('should display navigation bar elements on header and footer', () => {
    const navBarItems = header.getNavBarItems();
    navBarItems.each(function (element) {
      expect(element.isDisplayed()).toBeTruthy();
    });
  });

  it('should change the color of the navigation bar items while mouse move', () => {
    const navBarItems = header.getNavBarItems();
    navBarItems.each(function (element) {
      browser.actions().mouseMove(element).perform();
      expect(element.getCssValue('color')).toEqual('rgba(238, 92, 38, 1)');
    });
  });
});
