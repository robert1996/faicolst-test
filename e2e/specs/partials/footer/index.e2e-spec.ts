import {Footer} from '../../../pages/partials/footer';
import {browser} from 'protractor';

describe('Footer', () => {
  let footer: Footer;

  beforeEach(() => {
    footer = new Footer();
    footer.navigateTo(footer.getAbsoluteUrl());
  });

  it('should display security note', () => {
    const security = footer.getSecuriyNote();
    expect(security.isDisplayed()).toBeTruthy();
  });

  it('should display kichink logo', () => {
    const kichinkLogo = footer.getKichinkLogo();
    expect(kichinkLogo.isDisplayed()).toBeTruthy();
  });

  it('should display copy right', () => {
    const copyRight = footer.getCopyRight();
    expect(copyRight.isDisplayed()).toBeTruthy();
  });

  it('should display social icons', () => {
    const socialIcons = footer.getSocialIcons();
    socialIcons.each(function (element) {
      expect(element.isDisplayed()).toBeTruthy();
    });
    expect(socialIcons.count()).toEqual(3);
  });
});

