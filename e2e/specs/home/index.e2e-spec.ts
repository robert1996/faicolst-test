import {HomePage} from '../../pages/home';
import {browser} from 'protractor';

describe('HomePage', () => {
  let homePage: HomePage;
  beforeEach(() => {
    homePage = new HomePage();
    homePage.navigateTo(homePage.getAbsoluteUrl());
  });

  it('should display the right count of the sliders', () => {
    const sliderItems = homePage.getSliderItems();
    const bullets = homePage.getBullets();
    const bulletsLength = bullets.count();
    expect(sliderItems.count()).toEqual(bulletsLength);
  });

  it('should display a part of the hero text on the header', () => {
    const heroTextUrban = homePage.getUrbanText();
    expect(heroTextUrban.isDisplayed()).toBeTruthy();
    expect(heroTextUrban.getText()).toContain('URBAN');
  });

  it('should display the second part of the hero text on the header', () => {
    const heroTextCollection = homePage.getCollectionText();
    expect(heroTextCollection.isDisplayed()).toBeTruthy();
    expect(heroTextCollection.getText()).toContain('COLLECTION');
  });

  it('should display the year under the hero text', () => {
    const year = homePage.getYear();
    expect(year.isDisplayed()).toBeTruthy();
    expect(year.getText()).toContain('2017');
  });

  it('should display the Discount banner', () => {
    const discount = homePage.getBanners();
    expect(discount.isDisplayed()).toBeTruthy();
  });

  it('should display transparent overlay while mouse over', () => {
    const discount = homePage.getBanners();
    const discountHover = homePage.getBannersHover();
    browser.actions().mouseMove(discount).perform();
    expect(discountHover.isDisplayed()).toBeTruthy();
  });

  it('should display New Products section', () => {
    const newProducts = homePage.getNewProductsSection();
    expect(newProducts.isDisplayed()).toBeTruthy();
  });

  it('should display Featured Products section', () => {
    const featuredProducts = homePage.getFeaturedProductsSection();
    expect(featuredProducts.isDisplayed()).toBeTruthy();
  });
});
