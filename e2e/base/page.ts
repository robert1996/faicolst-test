import {browser} from 'protractor';

export class Page {
  public BASE_URL: string = 'http://localhost:4200';

  public navigateTo(url: string): any {
    return browser.get(url);
  }

  public getUrl(): any {
    return browser.getCurrentUrl();
  }
}
