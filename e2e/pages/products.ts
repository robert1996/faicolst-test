import {browser, element, by} from 'protractor';
import {Page} from '../base/page';

export class ProductsPage extends Page {
  public url: string = '/products';

  public getAbsoluteUrl() {
    return this.BASE_URL + this.url;
  }

  public getImage() {
    return element(by.css('.product-image'));
  }

  public getHover() {
    return element(by.css('.img-hover'));
  }

  public getHoverButton() {
    return element(by.css('p.img-descript'));
  }

  public getImageTitle() {
    return element(by.css('.product-title'));
  }

  public getPrice() {
    return element(by.css('.product-price'));
  }

  public getRating() {
    return element(by.css('.stars.product-rating'));
  }
}
