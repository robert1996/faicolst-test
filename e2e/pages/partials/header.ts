import {element, by} from 'protractor';
import {Page} from '../../base/page';

export class Header extends Page {
  public getAbsoluteUrl() {
    return this.BASE_URL;
  }

  public getLogo() {
    return element.all(by.css('.logo'));
  }

  public getHeaderItems() {
    return element.all(by.css('.header-item'));
  }

  public getNavBarItems() {
    return element.all(by.css('.nav-bar-item'));
  }
};
