import {element, by} from 'protractor';
import {Page} from '../../base/page';

export class Footer extends Page {
  public getAbsoluteUrl() {
    return this.BASE_URL;
  }

  public getKichinkLogo() {
    return element(by.css('.kichink-logo'));
  }

  public getSecuriyNote() {
    return element(by.css('.security-note'));
  }

  public getCopyRight() {
    return element(by.css('.copy-right'));
  }

  public getSocialIcons() {
    return element.all(by.css('.social-icon'));
  }
};


