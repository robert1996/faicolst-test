import {element, by} from 'protractor';
import {Page} from '../base/page';


export class HomePage extends Page {
  public getAbsoluteUrl() {
    return this.BASE_URL;
  }

  public getSliderItems() {
    return element.all(by.css('.slider-item'));
  }

  public getBullets() {
    return element.all(by.css('.bullet'));
  }

  public getUrbanText() {
    return element(by.css('h1.hero-text-urban'));
  }

  public getCollectionText() {
    return element(by.css('h1.hero-text-collection'));
  }

  public getYear() {
    return element(by.css('p.year'));
  }

  public getBanners() {
    return element(by.css('.banner'));
  }

  public getBannersHover() {
    return element(by.css('.img-descript'));
  }

  public getNewProductsSection() {
    return element(by.css('.new-product'));
  }

  public getFeaturedProductsSection() {
    return element(by.css('.featured-product'));
  }
}
