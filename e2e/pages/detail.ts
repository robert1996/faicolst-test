import {element, by} from 'protractor';
import {Page} from '../base/page';

export class DetailsPage extends Page {
  public url: string = '/products/details/1';

  public getAbsoluteUrl() {
    return this.BASE_URL + this.url;
  }

  public getProductInfo() {
    return element.all(by.css('.product-info'));
  }

  public getItemMiniatures() {
    return element.all(by.css('.img-miniature'));
  }

  public getItemImage() {
    return element(by.css('.item-picture'));
  }

  public getRelatedProcuctsSection() {
    return element(by.css('.featured-product'));
  }
}
